import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {Text} from 'react-native'
import {applyMiddleware, createStore} from 'redux';
import ReduxThunk from 'redux-thunk'
import reducers from './reducers'
import App from '../App.js';

export default class Store extends Component {
   
    render() {
        return (
            <Provider store={store}>
               <App/>
            </Provider>
        );
    }
}

const store = createStore(reducers,{},applyMiddleware(ReduxThunk));

