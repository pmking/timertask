const initial_state = {
    minutes:0,
    seconds:60,
    text:''
};

export default (state = initial_state, action) => {
    console.log(action)

    switch (action.type) {
        case 'UPDATE_VALUE':
            return { ...state, minutes:action.payload.minutes ,
                 seconds:action.payload.seconds,
                 text:action.payload.text
             }

        default:
            return state
    }
}