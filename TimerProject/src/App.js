/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Button} from 'react-native';
import {connect} from 'react-redux'
import {updateValue} from './redux-component/Action'


class App extends Component {

  constructor(props){
    super(props)
    this.state = {
      minute:0,
      seconds:60,
      text:''
    }
    global.x=''
    global.promise=''
  }

  componentDidMount(){
    
    
    promise =(time,data)=> new Promise(
      
      
      (resolve,reject)=>{
        var  now= new Date().getTime()
        var countDownTime = time => new Date(now+time*60*1000)
      
        this.props.updateValue({text:data,minutes:0,seconds:60})

        x = setInterval(()=>{
          var distance = countDownTime(time) - new Date().getTime()
    
        this.props.updateValue({
          text:data,
          minutes:Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)),
          seconds:Math.floor((distance % (1000 * 60)) / 1000)
        })
    
        if(distance<0){
          
          this.props.updateValue({
            minutes:0,
            seconds:0,
            text:data
          })
          resolve()
          clearInterval(x)
        }
        },1000)
      }
    )
    
    this.startTimer()

  }

  cleartimer(){
    clearInterval(x)
    x = null
    this.props.updateValue({
      minutes:0,
      seconds:60,
      text:'resetting'
    })
  }

  startTimer(){
    promise(5, 'First timer start').then(
      ()=>{
        promise(5,'Second timer start').then(()=>this.props.updateValue({
          text:'Timer finish',
          minutes:0,
          seconds:0
        }))
      }
    )
  }

  
  render() {
    return (
      <View style={{flex:0.5,justifyContent:'center'}}>
      <View style={styles.container}>
        <Text style={styles.welcome}>{this.props.minutes}</Text>
        <Text style={styles.welcome}>:</Text>
        <Text style={styles.welcome}>{this.props.seconds}</Text>
      </View>

      <Text style={styles.welcome}>{this.props.text}</Text>

      <Button onPress={()=>{
        this.cleartimer()
        this.startTimer()
    }}
         title="RESET TIMER"
         color="#841584"
         accessibilityLabel="Learn more about this purple button"
      />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    flexDirection:'row'
  },
  
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

const mapStateToProps = (state) =>{
  const {minutes,seconds,text} = state.data
  console.log(minutes,seconds,text)
  return {minutes,seconds,text}
}

export default connect(mapStateToProps,{updateValue})(App)

