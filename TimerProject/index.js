/** @format */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import Store from './src/redux-component/Store'

AppRegistry.registerComponent(appName, () => Store);
